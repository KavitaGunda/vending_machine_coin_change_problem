var app = angular.module("vendingMachineApp", ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
        $urlRouterProvider.otherwise('/home');
        $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'views/home.html',
            controller: 'VendingMachineController',
            controllerAs: 'vendingMachineCtrl'
        });
        $locationProvider.html5Mode(true);
});
