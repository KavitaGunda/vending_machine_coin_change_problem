app.controller('VendingMachineController', ['$scope', '$http', function($scope, $http) {
    $scope.data = [];
    $scope.message = "Vending machine !!!!!";
    $scope.part1_input = "";
    $scope.part1_output = "";
    $scope.part2_input = "";
    $scope.part2_output = "";

    $http({
        method: 'GET',
        url: '/getStock'
    }).then(function successCallback(response) {
        $scope.data = response.data;
    }, function errorCallback(response) {
        console.log('Error: ' + response);
        $scope.data = [];
    });

    // fetches change for problems statement (I)
    $scope.part1_make_change = function(input){
        var denominations = [];
        $scope.data.forEach(function(ele,index){
          denominations.push(ele.coin);
        });

      if(input && denominations && denominations.length > 0){
        console.log(input);
        $http({
            method: 'POST',
            url: '/calculateChange',
            data: { total: input, denominations:denominations }
        }).then(function successCallback(response) {
            $scope.part1_output = response.data;
        }, function errorCallback(response) {
            console.log('Error: ' + response);
            $scope.part1_output = [];
        });

      }
    };

    // fetches change for problems statement (II)
    $scope.part2_make_change = function(input){
        var denominations = []; var availablity = [];
          $scope.data.forEach(function(ele,index){
            denominations.push(ele.coin);
            availablity.push(ele.totalCount);
        });

      if(input && denominations && denominations.length > 0){
        console.log(input);
        $http({
            method: 'POST',
            url: '/calculate',
            data: { total: input, coins:denominations, availablity: availablity}
        }).then(function successCallback(response) {
            $scope.part2_output = response.data.change;
            $scope.data = response.data.rows;
        }, function errorCallback(response) {
            $scope.part2_output = response.data;
        });

      }
    };

    // Increment the stock
    $scope.increment = function(indx){
      $scope.data[indx].totalCount++;
      $http({
          method: 'POST',
          url: '/updateStock',
          data: { coin: $scope.data[indx].coin, totalCount: $scope.data[indx].totalCount}
      }).then(function successCallback(response) {
          console.log("successfully updated!");
      }, function errorCallback(response) {
          console.log('Error: ' + response);
          console.log("not updated successfully!");
      });
    };

}]);
