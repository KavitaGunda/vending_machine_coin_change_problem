# Vending Machine - coin change problems solver

Vending machine return the optimal change for a given number of pence as stated in the part (I), part (II), part (III) of the given problem statements.

Part (II) is dynamic inventory manager and pence dispencer, the amount is updated/decremented (when pence are dispenced from vending machine).

And there is also a provision of adding in stock of pence (database gets updated).

Part (I) is dynamically dispences pence irrespective of the stock, as mentioned in the problem statement. It results into a array of coins and its number of occurence in dispencing the amount.

## Features

**Server-side**
* [x] **[Node.JS](https://nodejs.org)** v6.x.x
* [x] **[Express](https://github.com/expressjs/express)**
* [x] [MongoDB](https://www.mongodb.com/)

**Client-side**
* [x] **[AngularJS 1.x](https://angularjs.org/)**

**Thirparty dependency modules/package**
* [x] **[bower]
* [x] **[gulp task runner]
* [x] **[Karma/Jasmine]

## Usage

Install dependencies

NOTE: node.js, mongodb, npm are basic dependencies required and pre installed on your system

Mongodb data dump files that are present in dump folder

```
mongorestore --collection inventory --db vending_machine dump/
```

Go to mongoshell check for the db
```
> use vending_machine
```

Please adjust the pence count as per the problem statement

```
$ npm install -g bower gulp
```

```
$ npm install -g karma-cli
```
```
$ npm install
```

```
$ bower install
```

Check for default tasks using:
```
$ gulp
```

Check for test cases using:
```
$ npm test
```
  OR
```
$ karma start karma.conf.js
```


Run the app using:
```
$ npm start
```

## Express API Endpoints @ http://localhost:3000/

To fetch the inventory use endpoint:
```
GET: /getStock
```

part (I) problem solver endpoint:
```
POST: /calculateChange		
```

part(II) problem solver endpoint:
```
POST: /calculate		
```

Increments or decrements coin count ednpoint:
```
PUT: /updateStock		
```


## Directory structure
```txt
+---build
+---bin
|	www.js
+---dump
|   +---vending_machine
+---models
|   inventory.js
+---public
|   +---images
|   +---javascript
|   |   +---controllers
|   |   |	   VendingMachineCntrl.js
|   |    app.js
|   +---libs
|   |   +---angular
|   |   +---angular-ui-router
|   |   +---bootstrap
|   +---stylesheets
|   |    style.css
|   +---views
|   |    home.html
|   |    index.html
+---routes
|   index.js
+---test
|   VendingMachineCntrl_spec.js
|
|
|   package.json
|   .bowerrc
|   app.js
|   app.js
|   bower.json
|   gulpfile.js
|   karma.conf.js
|   README.md

```
