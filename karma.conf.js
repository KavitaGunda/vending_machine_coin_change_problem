module.exports = function(config) {
  config.set({
    browsers: ['Chrome'],
    frameworks: ['jasmine'],
    files: [
      'public/libs/angular/angular.js',
      'public/libs/angular-ui-router/release/angular-ui-router.min.js',
      'public/javascripts/**/*.js',
      'test/**/*.js'
    ],
    exclude: [
    ],
    preprocessors: {},
    reporters: ['dots'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    captureTimeout: 60000,
    singleRun: true
    });
};
