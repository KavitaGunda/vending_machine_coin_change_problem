var gulp = require("gulp");
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var cssMin = require("gulp-css");
var ngAnnotate = require('gulp-ng-annotate');

//concats css files aand minifies them
gulp.task("css", function(){
    gulp.src([
      'public/libs/bootstrap/dist/css/bootstrap.css',
      'public/stylesheets/style.css'
    ])
    .pipe(concat("app.min.css"))
    .pipe(cssMin())
    .pipe(gulp.dest("public/stylesheets"));
});

//concats js files aand minifies them (thirdparty and custom)
gulp.task("scripts", function(){
  gulp.src([
    "public/libs/angular/angular.js",
    "public/libs/angular-ui-router/release/angular-ui-router.js"
  ])
  .pipe(concat("libs.js"))
  .pipe(ngAnnotate())
  .pipe(uglify())
  .pipe(gulp.dest("public/libs"));

  gulp.src([
    "public/javascripts/app.js",
    "public/javascripts/controllers/VendingMachineCntrl.js"
  ])
  .pipe(concat('main.js'))
  .pipe(ngAnnotate())
  .pipe(uglify())
  .pipe(gulp.dest("public/javascripts"));
});

gulp.task("default", ["css", "scripts"]);
