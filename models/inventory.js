var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost/vending_machine';

module.exports.getStock = function(callback) {

  MongoClient.connect(url, function(err, db) {
    if(err){
      callback({"code" : 100, "status" : "Error in connection database"});
    }else{
      var cursor = db.collection('inventory').find().toArray(function(err, items) {
        if(err){
          callback({"code" : 500, "status" : "Error in querying the database"});
        }else{
          callback(null, items);
        }
      });
    }
  });

};



module.exports.updateStock = function(coin, count, callback) {
  if(!coin && !count){
    callback({"code" : 100, "status" : "Arguments are empty"});
  }
  MongoClient.connect(url, function(err, db) {
    if(err){
      callback({"code" : 100, "status" : "Error in connection database"});
    }else{
        var cursor = db.collection('inventory').update({coin: coin}, { $set: {totalCount: count}}, function(err, items) {
        if(err){
          callback({"code" : 100, "status" : "Error in querying the database"});
        }else{
          console.log("Items updated");
          console.log(items);
          console.log(coin);
          console.log(count);
          callback(null, items);
        }
      });
    }
  });

};
