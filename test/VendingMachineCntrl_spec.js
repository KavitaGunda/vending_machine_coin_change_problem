// NOT COMPLETE
var $rootScope,
    $scope,
    controller;

var data = {};
var coinsArray = [{coin: 100, totalCount: 11}, {coin: 50, totalCount: 24},
  {coin: 20, totalCount: 11}, {coin: 10, totalCount: 11},
  {coin: 5, totalCount: 11}, {coin: 2, totalCount: 11}, {coin: 1, totalCount: 11}];

beforeEach(function(){
  module("vendingMachineApp");

  $inject(function($injector){
    $rootScope = $injector.get("rootScope");
    $scope = $rootScope.$new();
    controller = $injector.get("$controller")("VendingMachineController", {$scope: $scope});
  });
});


describe("Action Handlers", function(){
  describe("part1_make_change", function(){
      it("should dispence inventory change", function(){
        expect($scope.data).toEqual([]);
        $scope.part1_make_change();
        expect($scope.data).toEqual([]);
      });
  });

  describe("part2_make_change", function(){
      it("should dispence inventory change", function(){
        expect($scope.data).toEqual(data);
        $scope.part2_make_change();
        expect($scope.data).toEqual(data);
      });
  });

  describe("increment", function(){
      it("should increment the totalCount", function(){
        var result = $scope.increment();
        expect(result).toEqual({});
      });
  });

});
