var express = require('express');
var router = express.Router();
var inventory = require('../models/inventory');


/**
 * fetches inventoru collection data
 */

router.get('/getStock', function(req, res, next) {
    inventory.getStock(function(error, rows) {
        if(error){
          res.json(error);
        }else{
          res.status(200).json(rows);
        }
    });
});


/**
 * getChange returns and Array containing the values of pence
 * equivalent to the change for a given transaction
 * @param {Number}  totalPayable the amount payable for a transaction
 * @param {Number}  cashPaid  the amount the customer hands over to pay
 * @param {Array}  coins the coins available in the inventory
 * @param {Array}  availablity the coins stock in the inventory

 * @returns {Array} [500,20,5] the array equivalent of the change
 */

getChange = function (totalPayable, cashPaid, coins, availablity, callback) {
    'use strict';

    if(totalPayable === 0 || isNaN(totalPayable) || isNaN(cashPaid)) {
      callback('total/paid cannot be zero', []);
    }

    if(cashPaid - totalPayable === 0 || totalPayable - cashPaid === 0) {
      callback('no change', []);
    }

    var change = [], length = coins.length,
    remaining = cashPaid - totalPayable; // we reduce this below

    for (var i = 0; i < length; i++) { // loop through array of notes & coins:
        var coin = coins[i];

        if(remaining/coin >= 1) { // check coin fits into the remaining amount
            var times = Math.floor(remaining/coin); // no partial coins
            var availablityFlag = true;
            for(var j = 0; j < times; j++) { // add coin to change array x times
                if(availablity[i] > 0){
                  change.push(coin);
                  remaining = remaining - coin; // reduce remaining amount by coin
                  availablity[i] = (availablity[i] - 1);
                }else{
                  availablityFlag = false;
                }
                console.log("remaining" , remaining);
                if(remaining === 0){
                  callback(null, {change: change, coins:coins, availablity: availablity});
                }

            }
        }

          if(i == (length-1) && remaining > 0){  // when pence not available in the stock
            console.log("reached no change");
            callback("no change");
          }

    }
};


/**
 * Calculates the change for part (I) problem
 */
router.post("/calculateChange", function(req, res, next){
  var body = req.body || undefined;

  if(body){
    var coins = body.denominations;
    var total = body.total;
    var dispatched = [];
    for( var i = 0;  i < coins.length;  i++ ) {
        dispatched[i] = 0;
    }

    for( var c = 0;  total > 0;  c++ ) {
        dispatched[c] = Math.floor( total / coins[c] );
        total -= dispatched[c] * coins[c];
    }

    var output_arr = [];
    coins.forEach(function(element,index){
      output_arr.push({"coin":element, "occurence":dispatched[index]});
    });

    res.json(output_arr);

  }else{
    res.json({"code" : 100, "status" : "Post data is empty"});
  }
});


/**
 * Calculates the change for part (II) problem
 */

router.post("/calculate", function(req, res, next){
  var body = req.body || undefined;
    if(body){
          var coins = body.coins;
          var availablity = body.availablity;
          var total = body.total;

          // console.log(coins);
          // console.log(availablity);
          // console.log("-----------------------------");

          getChange(null, total, coins, availablity, function(err, data){
            if(err){
              console.log("inside getchange");
              console.log(err);
              res.status(500).json({"code" : 500, "status" : err});
            }else{

              for(var i = 0; i < data.availablity.length; i++){
                var ok = inventory.updateStock(data.coins[i], data.availablity[i], function(error, rows) {
                    if(error){
                      return "not ok";
                    }else{
                      return "ok";
                    }
                });
              }

              inventory.getStock(function(error, rows) {
                  if(error){
                    res.json(error);
                  }else{
                    data.rows = rows;
                    res.status(200).json(data);
                  }
              });

            }
          });

    }else{
      res.json({"code" : 400, "status" : "Post data is empty"});
    }
});



/**
 * Increments or decrements the coin count in the inventory
 */
router.post("/updateStock", function(req, res, next){
  var body = req.body || undefined;
  if(body){
    inventory.updateStock(body.coin, body.totalCount, function(error, rows) {
        if(error){
          res.json(error);
        }else{
          res.status(200).json(rows);
        }
    });
  }else{
    res.json({"code" : 400, "status" : "Post data is empty"});
  }
});


module.exports = router;
